import numpy as np
import scipy.misc
import scipy.signal
import skimage as sc



def lesBilde(path):
    """
    Leser inn og konverterer bildet om til float [0,1] verdier.

    Args:
        path: Bilde destinasjonen.
    
    Returns:
        Returnerer ferdig innlest bildet.
    """
    return scipy.misc.imread(path).astype(float)/255


def poisson(im, h, dt=0.24, dx=1):
    """
    Hovedfunksjonen til alle beregningene. Er ikke egnet til direkte bruk.
    Brukes vha. andre funksjoner.

    Args:
        im: Bildet som skal behandles.
        h: Matrisen som skal anvendes på bildet.
        dt: Tidsdifferanse, skal holdes <0.25 av stabilitetårsaker.
        dx: Pikseldifferansen under beregning.
    
    Returns:
        Returnerer ingenting, jobber på originalbildet.
    """
    laplac = laplace(im)        # Beregner Laplacen
    alpha = (dt/(dx**2))        # Beregner alpha
    im += alpha*(laplac - h)    # Beregner ligningen og anvender resultatet
    im[im<0] = 0                # Klipper verdier utenfor definisjonsområdet
    im[im>1] = 1                # 


def laplace(im):
    """
    Beregner Laplacen til bildet.

    Args:
        im: Bildet som det skal lages Laplacen til.
            
    Returns:
        Returnerer Laplacen til bildet.
    """
    laplac = np.zeros(im.shape)             # Allokerer plass
    laplac[1:-1, 1:-1] = (im[0:-2, 1:-1]    # Beregner laplacen, piksel over
                       + im[2:, 1:-1]       # Piksel under
                       + im[1:-1, 0:-2]     # Piksel til venstre
                       + im[1:-1, 2:]       # Piksel til høyre
                       - 4*im[1:-1, 1:-1])  # Piksel i midten
    return laplac                           # Returnerer resultatet


def gradient(im, direction, approximation):
    """
    Lager gradienten til bildet og returnerer resultatet.

    Args:
        im: Bildet som det skal beregnes gradienten til.
        direction: Gradientens retning ('x' / 'y').
        approximation: Måten gradienten beregnes på ('forward', 'backward'),
        har ikke noe å si dersom man skal ha gradienten til vanlig bildet.
        Om man vil ha divergensen til gradienten må da verdiene være
        forskjellige i hver bruk for å få sentrert differansen av andre orden.
    
    Returns:
        Returnerer gradienten, påvirker ikke originalbildet.
    """
    g = np.zeros(im.shape)                      # Allokerer plass
    if approximation=='forward':
        if direction=='x':
            g[:,:-1] = im[:, 1:] - im[:, :-1]   # Gradient x-retning
            g[:, -1] = g[:, -2]                 # Neumann randverdier
        elif direction=='y':
            g[:-1,:] = im[1:, :] - im[:-1,:]    # Gradient y-retning
            g[-1, :] = g[-2, :]                 # Neumann randverdier
        
    elif approximation=='backward':
        if direction=='x':
            g[:, 1:] = im[:, 1:] - im[:, :-1]   # Gradienten x-retning
            g[:, 0] = g[:, 1]                   # Neumann randverdier
        elif direction=='y':
            g[1: ,:] = im[1:, :] - im[:-1,:]    # Gradienten y-retning
            g[0, :] = g[1, :]                   # Neumann randverdier
    
    return g                                    # Returnerer gradienten


def randverdier(im, k=1, u0=None):
    """
    Anvender randverdier på et bilde.

    Args:
        im: Bildet som skal behandles.
    
    Returns:
        Returnerer ingenting, jobber på originalbildet.
    """
    # Neumann randverdier
    if k==1:
        im[:, 0] = im[:, 1]
        im[:, -1] = im[:, -2]    
        im[0, :] = im[1, :]      
        im[-1, :] = im[-2 , :]
    
    # Neumann randverdier for kontrastforsterkning   
    else:
        im[:, 0] = im[:, 1] + k*(u0[:, 0] - u0[:, 1])      
        im[:, -1] = im[:, -2] + k*(u0[:, -1] - u0[:, -2])
        im[0, :] = im[1, :] + k*(u0[0, :] - u0[1, :])
        im[-1, :] = im[-2 , :] + k*(u0[-1, :] - u0[-2 , :])


def glattingExp(im, n, dt=0.24, rand='N'):
    """
    Glatter bildet send som parameter.

    Args:
        im: Bildet som skal behandles.
        n: Antall ganger vi skal anvende beregningene på bildet.
        dt: Tidsdifferanse, skal holdes <0.25 av stabilitetårsaker.
        rand: Randverdier ('D': Dirichlet, 'N':Neumann)
    
    Returns:
        Returnerer ingenting, jobber på originalbildet.
    """
    if len(im.shape) != 2 and im.shape[2] != 3:
        raise ValueError('Feil dimensjoner på bildet')
    if n<0:
        raise ValueError('n må være større enn eller lik null!')

    for _ in range(n):
        poisson(im, 0, dt)
        if rand=='N':
            randverdier(im)
        # else Dirichlet, vi får samme resultatet om vi ikke gjør noe
        # Det skyldes måten vi anvender glatting på, alle pikselene
        # uten om randpikselene blir påvirket av glatting, så pikselene på
        # kanten står igjen fra originalbildet og resultatet blir helt det
        # samme som om vi hadde anvendet Dirichlet



def kantbevarendeGlatting(img, k, n, dt=0.24, rand='N'):
    """
    Glatter hele bildet uten å gjøre kanter uskarpe.

    Args:
        img: Bildet som skal behandles.
        k: Positiv konstant som angir dempningen på kantene.
        n: Antall ganger vi skal anvende beregningene.
        dt: Tidsdifferanse, skal holdes <0.25 av stabilitetårsaker.
    
    Returns:
        Returnerer glattet bildet med skarpe kanter.
    """
    
    if len(img.shape) != 2 and img.shape[2] != 3:
        raise ValueError('Feil dimensjoner på bildet')
    if k<0:
        raise ValueError('k må være større enn eller lik null')
    if n<0:
        raise ValueError('n må være større enn eller lik null')
    if dt<0 or dt>=0.25:
        raise ValueError('dt må være i intervallet [0, 0.25> ')
    
    im = img.copy()
    u0x = gradient(im, 'x', 'forward')        # Beregner gradienten i x-retning
    u0y = gradient(im, 'y', 'forward')        # og y-retning
    ul = (u0x**2 + u0y**2)                    # Gradientens lengde
    D = 1/(1+(k*ul))                          # Regner ut Diffusjonskonstanten                             

    for _ in range(n):
        ux = gradient(im, 'x', 'forward')     # Gradienten til bildet i x-retning
        uy = gradient(im, 'y', 'forward')     # Gradienten til bildet i y-retning
        ux, uy = D*ux, D*uy                   # Anvender Diffusjonskonstanten
                
        uxx = gradient(ux, 'x', 'backward')   # Gradienten til gradienten i x-retning
        uyy = gradient(uy, 'y', 'backward')   # Gradienten til gradienten i y-retning
        h = uxx + uyy                         # Legger sammen andregrads og får Laplacen
        
        im += h*dt                            # Anvender Laplacen på bildet

        if rand=='N':                         
            randverdier(im)
        # else: se glatting()
    
    im[im>1] = 1                              # Klipper verdiene
    im[im<0] = 0                              #  
    return im                                 # Returnerer resultatet


def kantbevarendeGlattingIL(img, k, n, rand='N'):
    """
    Glatter hele bildet uten å gjøre kanter uskarpe.

    Args:
        img: Bildet som skal behandles.
        k: Positiv konstant som angir dempningen på kantene.
        n: Antall ganger vi skal anvende beregningene.
        dt: Tidsdifferanse, skal holdes <0.25 av stabilitetårsaker.
    
    Returns:
        Returnerer glattet bildet med skarpe kanter.
    """
    
    if len(img.shape) != 2 and img.shape[2] != 3:
        raise ValueError('Feil dimensjoner på bildet')
    if k<0:
        raise ValueError('k må være større enn eller lik null')
    if n<0:
        raise ValueError('n må være større enn eller lik null')
                              
    im = img.copy()
    for _ in range(n):
        ux = gradient(im, 'x', 'forward')     # Gradienten til bildet i x-retning
        uy = gradient(im, 'y', 'forward')     # Gradienten til bildet i y-retning
        ul = (ux**2 + uy**2)                    # Gradientens lengde
        D = 1/(1+(k*ul))                          # Regner ut Diffusjonskonstanten   
        ux, uy = D*ux, D*uy                   # Anvender Diffusjonskonstanten
                
        uxx = gradient(ux, 'x', 'backward')   # Gradienten til gradienten i x-retning
        uyy = gradient(uy, 'y', 'backward')   # Gradienten til gradienten i y-retning
        h = uxx + uyy                         # Legger sammen andregrads og får Laplacen
        
        dt = (D/4)-0.01
        im += h*dt                            # Anvender Laplacen på bildet

        if rand=='N':                         
            randverdier(im)
        # else: se glatting()
    
    im[im>1] = 1                              # Klipper verdiene
    im[im<0] = 0                              #
    return im                                 # Returnerer resultatet
